package main

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

var channels map[string]*discordgo.Channel

func loadChannels() {
	channels = make(map[string]*discordgo.Channel)

	items, err := discord.GuildChannels(libriaGuildID)
	if err != nil {
		log.Fatal(err)
	}

	for _, item := range items {
		channels[item.ID] = item
	}
}
