package event

import (
	"log"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/rs/xid"
)

var subscription = make(map[xid.ID]*Event)

type Context struct {
	RegisterAt time.Time
	StartedAt  time.Time
	Message    *discordgo.MessageCreate
	To         string
	Params     []interface{}
}

type Event struct {
	id      xid.ID
	timeout time.Duration
	once    bool

	Event     string
	IsTimeout bool
	Complete  bool
	Context   *Context

	params    chan []interface{}
	done      chan *Event
	terminate chan *Event

	thenFunc    func(event *Event)
	timeoutFunc func(event *Event)
	matchFunc   func(event *Event) bool
}

func (e *Event) insert() {
	subscription[e.id] = e
}

func (e *Event) remove() {
	delete(subscription, e.id)
}

func (e *Event) check(wg *sync.WaitGroup, event string, events *[]*Event) {
	defer wg.Done()
	if event == e.Event {
		*events = append(*events, e)
	}
}

func (e *Event) emit(ctx *Context, args []interface{}) {
	e.Complete = true
	e.Context = ctx
	e.params <- args
}

func (e *Event) doDone() {
	if e.done != nil {
		e.done <- e
	}
}

func (e *Event) doTimeout() {
	if e.timeoutFunc != nil {
		e.IsTimeout = true
		e.timeoutFunc(e)
		e.doDone()
		e.remove()
	}
}

func (e *Event) doMatch() bool {
	if e.matchFunc != nil {
		return e.matchFunc(e)
	}
	return true
}

func (e *Event) doThen() {
	defer e.doDone()

	if e.thenFunc == nil {
		return
	}
	e.thenFunc(e)
	log.Printf("[EVENT] %s: %s %s", e.id, time.Now().Sub(e.Context.StartedAt).String(), e.Event)
}

func (e *Event) doTerminate() {
	e.remove()
	log.Printf("[EVENT] %s terminated: %s %s", e.id, time.Now().Sub(e.Context.StartedAt).String(), e.Event)
}

func (e *Event) Channel(done chan *Event) *Event {
	e.done = done
	return e
}

func (e *Event) Once(once bool) *Event {
	e.once = once
	return e
}

func (e *Event) Then(callback func(event *Event)) *Event {
	e.thenFunc = callback
	return e
}

func (e *Event) Timeout(timeout time.Duration, callback func(event *Event)) *Event {
	e.timeout = timeout
	e.timeoutFunc = callback
	return e
}

func (e *Event) Match(callback func(event *Event) bool) *Event {
	e.matchFunc = callback
	return e
}

func (e *Event) Sync() {
	e.once = true
	e.insert()
	defer e.remove()

	var timeout <-chan time.Time
	if e.timeoutFunc != nil {
		timeout = time.After(e.timeout)
	}

	for {
		select {
		case <-timeout:
			e.doTimeout()
			return
		case e.Context.Params = <-e.params:
			if !e.doMatch() {
				continue
			}
			e.doThen()
			if e.Complete {
				return
			}
		case <-e.terminate:
			e.doTerminate()
			return
		}
	}
}

func (e *Event) Async() {
	e.insert()

	var timeout <-chan time.Time
	if e.timeoutFunc != nil {
		timeout = time.After(e.timeout)
	}

	do := func() {
		for {
			select {
			case <-timeout:
				e.doTimeout()
				e.doTerminate()
				return
			case args := <-e.params:
				e.Context.Params = args
				if !e.doMatch() {
					continue
				}
				if e.timeoutFunc != nil {
					timeout = time.After(e.timeout)
				}
				e.doThen()
			case <-e.terminate:
				e.doTerminate()
				return
			}
		}
	}

	go do()
}

func (e *Event) Terminate() {
	e.terminate <- e
}

func On(event string) *Event {
	return &Event{
		id:        xid.New(),
		Event:     event,
		once:      false,
		params:    make(chan []interface{}),
		terminate: make(chan *Event),
	}
}

func events(event string) []*Event {
	wg := sync.WaitGroup{}
	wg.Add(len(subscription))

	result := make([]*Event, 0)

	for _, e := range subscription {
		go e.check(&wg, event, &result)
	}

	wg.Wait()
	return result
}
