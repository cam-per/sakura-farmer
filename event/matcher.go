package event

import (
	"fmt"
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
)

var matchers = make(map[string]*matcher)

var event chan string

type matcher struct {
	event string
	f     string

	in chan *discordgo.MessageCreate

	processFunc func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool)
}

func (m *matcher) format(format string) *matcher {
	m.f = format
	return m
}

func (m *matcher) process(p func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool)) *matcher {
	m.processFunc = p
	return m
}

func (m *matcher) start() {
	f := func(m *matcher) {
		for s := range m.in {
			ctx := Context{
				StartedAt: time.Now(),
				Message:   s,
			}
			params, ok := m.processFunc(&ctx, m, s)
			if !ok {
				continue
			}
			events := events(m.event)
			for _, e := range events {
				go e.emit(&ctx, params)
			}
		}
	}

	go f(m)
}

func (m *matcher) match(mc *discordgo.MessageCreate) {
	m.in <- mc
}

func match(event string) *matcher {
	m := &matcher{
		event: event,
		in:    make(chan *discordgo.MessageCreate),
	}
	matchers[event] = m
	return m
}

func Emit(mc *discordgo.MessageCreate) {
	for _, m := range matchers {
		go m.match(mc)
	}
}

func init() {
	match("sakura.in").
		format("%d случайных 🌸 появились! Напишите `.pick и код с картинки`, чтобы собрать их.").
		process(func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool) {
			var currency int
			n, err := fmt.Sscanf(mc.Content, m.f, &currency)
			if n != 1 || err != nil {
				return nil, false
			}

			attachments := mc.Attachments

			if len(attachments) != 1 {
				return nil, false
			}

			result := make([]interface{}, 0)
			result = append(result, currency)
			result = append(result, attachments[0].URL)
			return result, true
		}).start()

	match("sakura.pick").
		format(".pick %s").
		process(func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool) {
			var code string
			n, err := fmt.Sscanf(mc.Content, m.f, &code)
			if n != 1 || err != nil {
				return nil, false
			}

			ctx.Message = mc

			result := make([]interface{}, 0)
			result = append(result, code)
			return result, true
		}).start()

	match("sakura.pick-success").
		format("**<@!%d>** собрал %d🌸").
		process(func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool) {
			var to uint64
			var currency int

			if len(mc.Embeds) != 1 {
				return nil, false
			}

			n, err := fmt.Sscanf(mc.Embeds[0].Description, m.f, &to, &currency)
			if n != 2 || err != nil {
				return nil, false
			}

			ctx.To = strconv.FormatUint(to, 10)
			ctx.Message = mc

			result := make([]interface{}, 0)
			result = append(result, currency)
			return result, true
		}).start()

	match("casino.timely-deny").
		format("**<@!%d>** Вы уже получили свою ежедневную награду. Вы сможете получить её заново через %dd %dh %dm %ds.").
		process(func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool) {
			var to uint64
			var days, hours, minutes, seconds int64

			if len(mc.Embeds) != 1 {
				return nil, false
			}

			n, err := fmt.Sscanf(mc.Embeds[0].Description, m.f, &to, &days, &hours, &minutes, &seconds)
			if n != 5 || err != nil {
				return nil, false
			}

			duration := time.Duration(24*days)*time.Hour +
				time.Duration(hours)*time.Hour +
				time.Duration(minutes)*time.Minute +
				time.Duration(seconds)*time.Second

			result := make([]interface{}, 0)
			result = append(result, duration)

			ctx.To = strconv.FormatUint(to, 10)
			ctx.Message = mc

			return result, true
		}).start()

	match("casino.timely-success").
		format("**<@!%d>** Вы получили свои 300🌸. Вы сможете получить снова через 6ч").
		process(func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool) {
			var to uint64

			if len(mc.Embeds) != 1 {
				return nil, false
			}

			n, err := fmt.Sscanf(mc.Embeds[0].Description, m.f, &to)
			if n != 1 || err != nil {
				return nil, false
			}

			ctx.To = strconv.FormatUint(to, 10)
			ctx.Message = mc

			return nil, true
		}).start()

	match("casino.$").
		format("У <@!%d> есть %d 🌸").
		process(func(ctx *Context, m *matcher, mc *discordgo.MessageCreate) ([]interface{}, bool) {
			var to uint64
			var currency uint64

			if len(mc.Embeds) != 1 {
				return nil, false
			}

			n, err := fmt.Sscanf(mc.Embeds[0].Description, m.f, &to, &currency)
			if n != 2 || err != nil {
				return nil, false
			}

			ctx.To = strconv.FormatUint(to, 10)
			ctx.Message = mc

			result := make([]interface{}, 0)
			result = append(result, currency)

			return result, true
		}).start()
}
