package main

import (
	"log"
	"math/rand"
	"time"

	"bitbucket.org/cam-per/sakura-farmer/event"
	"bitbucket.org/cam-per/sakura-farmer/ocr"
)

func sakuraIn() {
	event.On("sakura.in").
		Then(func(e *event.Event) {
			currency := e.Context.Params[0].(int)
			url := e.Context.Params[1].(string)

			resp, err := discord.Client.Get(url)
			if err != nil {
				log.Printf("[SAKURA] Picking %d fail. %s", currency, err)
				return
			}
			defer resp.Body.Close()

			text, err := ocr.Text(resp.Body)
			if err != nil {
				log.Printf("[SAKURA] Picking %d fail. %s", currency, err)
				return
			}

			if e.Context.Message.ChannelID != tavernaChannelID {
				return
			}

			pause := time.Duration(1500)*time.Millisecond + time.Duration(rand.Int31n(1500))*time.Millisecond
			time.Sleep(pause)

			// _, err = discord.ChannelMessageSend(e.Context.Message.ChannelID, ".pick "+text)
			// if err != nil {
			// 	log.Println(err)
			// }
			log.Printf("[SAKURA] Picking %d success. Code: %s. Sended %s", currency, text, time.Since(e.Context.StartedAt).String())
		}).Async()
}
