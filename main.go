package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/cam-per/sakura-farmer/event"
	"bitbucket.org/cam-per/sakura-farmer/ocr"

	"github.com/bwmarrin/discordgo"
)

const (
	libriaGuildID = "319073776383819776"

	tavernaChannelID = "389413733329272837"
	casinoChannelID  = "423480105516662784"

	libriaChanBotID = "537168590819033088"
)

var err error
var discord *discordgo.Session
var me *discordgo.User

func main() {
	var (
		login    = flag.String("l", "", "Discord login")
		password = flag.String("p", "", "Discord password")
	)
	flag.Parse()

	rand.Seed(time.Now().Unix())

	ocr.Init()

	discord, err = discordgo.New(*login, *password)
	if err != nil {
		log.Fatal(err)
	}

	me, err = discord.User("@me")
	if err != nil {
		log.Fatal(err)
	}

	loadChannels()

	discord.AddHandler(messageCreate)

	err = discord.Open()
	if err != nil {
		log.Fatal(err)
		return
	}

	sakuraIn()

	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	discord.Close()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if m.GuildID != libriaGuildID {
		return
	}

	event.Emit(m)
}
