package ocr

import (
	"bufio"
	"log"
	"os"
	"strings"
)

var correctionFile *os.File
var correctionMap = map[rune]rune{}

func check(ch rune) bool {
	const chrs = "0123456789abcdef"
	return strings.ContainsRune(chrs, ch)
}

func exclude(ch rune) bool {
	const chrs = " ~-+*/_`$#&.,?"
	return strings.ContainsRune(chrs, ch)
}

func correct(text string) (string, bool) {
	var result string
	success := true

	for _, c := range text {
		if exclude(c) {
			continue
		}

		if check(c) {
			result += string(c)
			continue
		}

		ch, ok := correctionMap[c]
		if ok {
			result += string(ch)
			continue
		}

		success = false
		result += string(c)
	}

	log.Println(len(result), result)

	if len(result) != 4 {
		return "", false
	}

	return result, success
}

func Correct(wrong, rigth string) {
	if len(wrong) != 4 || len(rigth) != 4 {
		return
	}

	for i, c := range wrong {
		if !check(c) {

			w := bufio.NewWriter(correctionFile)
			if n, err := w.WriteString(string(c) + string(rigth[i]) + "\n"); err != nil {
				log.Println(n, err)
			}

			if err := w.Flush(); err != nil {
				log.Println(err)
			}

			if _, exists := correctionMap[c]; exists {
				continue
			}
			correctionMap[c] = rune(rigth[i])
		}
	}
}
