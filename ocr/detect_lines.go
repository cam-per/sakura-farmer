package ocr

import (
	"image"
	"sync"
)

func deteLines(img image.Image) [2][2]int {
	width := img.Bounds().Max.X - 1
	heigth := img.Bounds().Max.Y - 1

	var result = [2][2]int{
		{-1, -1},
		{-1, -1},
	}

	detect := func(wg *sync.WaitGroup, x, y1, y2 int, result *int) {
		defer wg.Done()
		var dy int

		if y1 < y2 {
			dy = 1
		} else {
			dy = -1
		}

		for y1 != y2 {
			if isBlack(img.At(x, y1)) {
				*result = y1
				return
			}
			y1 += dy
		}
	}

	wg := sync.WaitGroup{}
	wg.Add(4)

	go detect(&wg, 0, 0, heigth, &result[0][0])
	go detect(&wg, 0, heigth, 0, &result[0][1])
	go detect(&wg, width, 0, heigth, &result[1][0])
	go detect(&wg, width, heigth, 0, &result[1][1])
	wg.Wait()

	return result
}
