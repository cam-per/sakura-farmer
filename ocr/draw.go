package ocr

import (
	"image"
	"image/color"
	"sync"

	"github.com/anthonynsimon/bild/clone"
)

func draw(img image.Image, lines [2][4]int) image.Image {
	width := img.Bounds().Max.X - 1
	heigth := img.Bounds().Max.Y - 1

	dst := clone.AsRGBA(img)

	line := func(wg *sync.WaitGroup, p [4]int) {
		defer wg.Done()

		plotLine(p, func(x, y int, opacity float64) {
			if !isBlack(dst.At(x, y)) {
				return
			}
			var up, down = false, false
			if y+1 <= heigth {
				if isBlack(dst.At(x, y+1)) {
					down = true
				}
			}
			if y-1 >= 0 {
				if isBlack(dst.At(x, y-1)) {
					up = true
				}
			}

			if up && down {
				return
			}

			dst.SetRGBA(x, y, color.RGBA{255, 255, 255, 255})
		})
	}

	for x := 0; x <= width; x++ {
		for y := 0; y <= heigth; y++ {
			if y <= 8 || y >= heigth-8 {
				dst.SetRGBA(x, y, color.RGBA{255, 255, 255, 255})
			}
		}
	}

	wg := sync.WaitGroup{}
	for i := 0; i < len(lines); i++ {
		wg.Add(1)
		go line(&wg, lines[i])
	}
	wg.Wait()
	return dst
}
