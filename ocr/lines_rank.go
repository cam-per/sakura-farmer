package ocr

import (
	"image"
	"sync"
)

func lineRanks(img image.Image, p [2][2]int) ([4][4]int, [4]float32) {
	width := img.Bounds().Max.X - 1

	var lines = [4][4]int{
		{0, p[0][0], width, p[1][0]},
		{0, p[0][0], width, p[1][1]},
		{0, p[0][1], width, p[1][0]},
		{0, p[0][1], width, p[1][1]},
	}
	var ranks [4]float32

	rank := func(wg *sync.WaitGroup, p [4]int, result *float32) {
		defer wg.Done()

		black := 0
		count := 0

		op := 0.0

		plotLine(p, func(x, y int, opacity float64) {
			count++
			if isBlack(img.At(x, y)) {
				black++
				op += opacity
			}
		})

		*result = float32(black) / float32(count)
	}

	wg := sync.WaitGroup{}
	for line := 0; line < len(lines); line++ {
		wg.Add(1)
		go rank(&wg, lines[line], &ranks[line])
	}
	wg.Wait()
	return lines, ranks
}
