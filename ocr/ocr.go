package ocr

import (
	"bufio"
	"bytes"
	"fmt"
	"image"
	"image/png"
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/anthonynsimon/bild/effect"
	"github.com/anthonynsimon/bild/segment"
	"github.com/anthonynsimon/bild/transform"
	"github.com/otiai10/gosseract"

	"github.com/anthonynsimon/bild/channel"
)

func Init() {
	err := os.MkdirAll(filepath.Join(".", "datasets"), os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}

	correctionFile, err = os.OpenFile(filepath.Join(".", "datasets", "correction.map"), os.O_CREATE|os.O_APPEND|os.O_RDWR, os.ModePerm)
	for {
		var data string
		_, err := fmt.Fscanln(correctionFile, &data)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		correctionMap[rune(data[0])] = rune(data[1])
	}
}

func Text(reader io.Reader) (string, error) {
	img, err := png.Decode(reader)
	if err != nil {
		return "", err
	}

	img = channel.Extract(img, channel.Alpha)
	w, h := size(img)
	img = transform.Crop(img, image.Rect(0, 0, w, h))
	img = segment.Threshold(img, 255)
	img = effect.Invert(img)

	img = preprocess(img)

	client := gosseract.NewClient()
	defer client.Close()

	client.SetVariable("load_system_dawg", "0")
	client.SetVariable("load_freq_dawg", "0")

	buff := bytes.NewBuffer(nil)
	writer := bufio.NewWriter(buff)

	err = png.Encode(writer, img)
	if err != nil {
		return "", err
	}

	err = writer.Flush()
	if err != nil {
		return "", err
	}
	bytes := buff.Bytes()
	client.SetImageFromBytes(bytes)

	text, err := client.Text()
	if err != nil {
		return "", err
	}

	corrected, ok := correct(text)
	if !ok {
		return corrected, fmt.Errorf("Text '%s' is incorrect", text)
	}

	return corrected, nil
}
