package ocr

import "math"

func plotLine(p [4]int, plot func(x, y int, opacity float64)) {
	round := func(x float64) int {
		return int(math.Round(x))
	}

	ipart := func(x float64) float64 {
		return float64(int64(x))
	}

	fpart := func(x float64) float64 {
		return x - ipart(x)
	}

	x1, y1, x2, y2 := float64(p[0]), float64(p[1]), float64(p[2]), float64(p[3])
	if x2 < x1 {
		x1, x2 = x2, x1
	}
	dx := x2 - x1
	dy := y2 - y1
	gradient := dy / dx

	xend := math.Round(x1)
	yend := y1 + gradient*(xend-x1)
	xgap := 1 - fpart(x1+0.5)
	xpxl1 := xend
	ypxl1 := ipart(yend)

	plot(round(xpxl1), round(ypxl1), (1-fpart(yend))*xgap)
	plot(round(xpxl1), round(ypxl1+1.0), fpart(yend)*xgap)

	intery := yend + gradient

	xend = math.Round(x2)
	yend = y2 + gradient*(xend-x2)
	xgap = fpart(x2 + 0.5)
	xpxl2 := xend
	ypxl2 := ipart(yend)

	plot(round(xpxl2), round(ypxl2), (1-fpart(yend))*xgap)
	plot(round(xpxl2), round(ypxl2+1), fpart(yend)*xgap)

	for x := xpxl1 + 1; x <= xpxl2-1; x++ {
		plot(round(x), round(ipart(intery)), 1-fpart(intery))
		plot(round(x), round(ipart(intery)+1), fpart(intery))
		intery += gradient
	}
}
