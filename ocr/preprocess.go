package ocr

import (
	"image"
)

func preprocess(img image.Image) image.Image {

	points := deteLines(img)
	l, r := lineRanks(img, points)

	var lines [2][4]int
	for it := 0; it < 2; it++ {
		index := 0
		max := r[0]
		for i := 1; i < len(r); i++ {
			if max < r[i] {
				max = r[i]
				index = i
			}
		}
		r[index] = 0.0
		lines[it] = l[index]
	}

	dst := draw(img, lines)
	return dst
}
