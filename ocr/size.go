package ocr

import (
	"image"
	"sync"
)

func size(img image.Image) (int, int) {
	wg := sync.WaitGroup{}
	wg.Add(2)

	var w, h int

	go func(wg *sync.WaitGroup, img image.Image) {
		defer wg.Done()

		y := img.Bounds().Min.Y
		for x := img.Bounds().Min.X; x < img.Bounds().Max.X; x++ {
			if isBlack(img.At(x, y)) {
				w = x - 1
				return
			}
		}
	}(&wg, img)

	go func(wg *sync.WaitGroup, img image.Image) {
		defer wg.Done()

		x := img.Bounds().Min.X
		for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y++ {
			if isBlack(img.At(x, y)) {
				h = y - 1
				return
			}
		}
	}(&wg, img)

	wg.Wait()
	return w, h
}
