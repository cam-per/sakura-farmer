package ocr

import (
	"image/color"
)

func isBlack(color color.Color) bool {
	r, g, b, _ := color.RGBA()
	return r+g+b == 0
}
